require "nokogiri"

html_file_path = ARGV[0]

doc = Nokogiri::HTML(File.read(html_file_path))
doc.xpath("//link[@type='text/css']").first["href"] = "style.css"
File.write(html_file_path, doc.to_html)
