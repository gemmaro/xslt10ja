index.pot: index.html
	html2po --pot $< $@

prior-ja.fix.po: prior-ja.fix.html
	html2po $< $@

prior-ja.fix.html: prior-ja.html
	ruby clean_html.rb < $< > $@

