require "nokogiri"

translated_document = ARGV[0]
document = Nokogiri::HTML(File.read(translated_document).encode(Encoding::UTF_8, "iso-8859-1"))
document.xpath("//html/head/meta/@content").first.content = "text/html; charset=UTF-8"
File.write(translated_document, document.to_xhtml)
