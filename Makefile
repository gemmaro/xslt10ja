all: translate public/style.css
.PHONY: all

translate: po4a.cfg index.xhtml
	mkdir -p public
	po4a $<
	ruby fix_style_path.rb public/index.html
.PHONY: translate

style: guix.scm
	guix style -f $<
.PHONY: style

serve:
	ruby -run -e httpd public
.PHONY: serve

index.xhtml: index.html
	cp $< $@
	ruby fix_encoding.rb $@

index.html:
	curl -o $@ https://www.w3.org/TR/1999/REC-xslt-19991116

prior-ja.xhtml: prior-ja.html
	xmllint --html --xmlout $< > $@

prior-ja.html:
	curl https://web.archive.org/web/20120606043037/http://www.infoteria.com/jp/contents/xml-data/REC-xslt-19991116-jpn.htm > $@

public/style.css:
	curl https://www.w3.org/StyleSheets/TR/W3C-REC > $@

clean:
	rm index.html index.xhtml
.PHONY: clean
