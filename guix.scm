(use-modules (guix packages)
             (guix git)
             (guix build-system trivial)
             ((guix licenses)
              #:prefix license:)
             (gnu packages)
             (gnu packages web)
             (gnu packages ruby)
             (gnu packages gettext)
             (gnu packages textutils)
             (gnu packages curl)
             (gnu packages lsof))

(package
  (name "xsltja")
  (version "0.1.0")
  (source (git-checkout (url (dirname (current-filename)))))
  (build-system trivial-build-system)
  (native-inputs (list tidy-html
                       ruby
                       ruby-nokogiri
                       nkf
                       curl
                       gettext
                       po4a
                       lsof))
  (home-page "TODO")
  (synopsis "TODO")
  (description "TODO")
  (license license:fdl1.3+))
